var kuromoji = require('kuromoji');

// この builder が辞書やら何やらをみて、形態素解析機を造ってくれるオブジェクトです。
var builder = kuromoji.builder({
  // ここで辞書があるパスを指定します。今回は kuromoji.js 標準の辞書があるディレクトリを指定
  // dicPath: 'node_modules/kuromoji/dict'
  dicPath: __dirname + '/dict'
});

// this uses the callback syntax, however, we encourage you to try the async/await syntax shown in async-dadjoke.js
export function handler(event, context, callback) {
  // 形態素解析機を作るメソッド
  builder.build(function (err, tokenizer) {
    // 辞書がなかったりするとここでエラーになります(´・ω・｀)
    if (err) { throw err; }
    let reqMsg =  event.queryStringParameters.msg || 'テスト'
    // tokenizer.tokenize に文字列を渡すと、その文を形態素解析してくれます。
    let result = tokenizer.tokenize(reqMsg);
    // console.dir(result);

    console.log('queryStringParameters', event.queryStringParameters)
    console.log('tokened:' + result)
    callback(null, {
      statusCode: 200,
      body: JSON.stringify({ msg: result }),
    })
  });

}





